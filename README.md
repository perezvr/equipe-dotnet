# Prova da Equipe .NET

## Tecnologias

- Desenvolvi uma aplicação .NET Core(3.1) WebApi utlizando DDD com EF Core.
- Para o banco de dados escolhi o SqlServer por se tratar de um banco altamente robusto e confiável para aplicações relacionais.
- Utilizei o gerenciamento de injeção de dependências do próprio .net core.
- A aplicação disponibiliza uma REST API para as 4 entidades solicitadas porém tive dificuldades de implantar o login com o JWT por exemplo pois me demandaria um pouco mais de tempo.
- Não desenvolvi o front-end pela mesma falta de tempo e por não dominar a ponto de apresetar uma aplicação profissional o suficiente.
- Acredito que as maiores dificuldades foram a falta de "fluencia" com aplicações front end e o curto espaço de tempo (no meu entendimento) para a realização das tarefas.
- Eu poderia partir para a abordagem de criar somente o domínio das tarefas e já passar os objetos todos preenchidos. Resolvi criar um domínio para cada uma das 4 entidades para que cada uma tivesse seu CRUD garantido, podendo incluir novas categorias e etc a partir de uma funcionalidade específica pra isso.